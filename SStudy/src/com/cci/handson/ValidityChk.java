package com.cci.handson;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ValidityChk {

	public static String PwdValidityChk(String str) {
		Pattern p = Pattern.compile("[%!@#$%^&*()_+:~,./;'=-]?[0-9]");
		Matcher m = p.matcher(str);
		String result;
		if (str.length() > 6) {
			if (m.find()) {
				result = "Valid Password";
			} else {
				result = "Invalid Password";
			}
		} else {
			result = "Length should me greater than 7";
		}
		return result;
	}
	public static String phoneCode(String str) {
		if(!str.startsWith("0"))
		{
			str = "Invalid number";
		}
		return str;
	}
	public static String removeSpclChar(String str) {
		str = str.replace(" ", "");
		return Pattern.compile("[%!@#$%^&*()_+:~,./;'=-]").matcher(str)
				.replaceAll("");
	}

	public static String checkZeros(String str) {
		if(!str.startsWith("0"))
		{
			str = "Invalid number";
		}
		return str;
	}

	public static void phoneCheckDetails(String str2) {
		if (str2.length() == 12) {
				phoneCode(str2.substring(0, 2));
			System.out.println("Country code : "
					+ CodeMap(phoneCode(str2.substring(0, 2))));
			System.out.println("Service provider : " + phoneCode(str2.substring(2, 6)));
			System.out.println("User Number : " + str2.substring(6, 12));
		} else if (str2.length() == 13) {
			System.out.println("Country code : "
					+ CodeMap(phoneCode(str2.substring(0, 3))));
			System.out.println("Service provider : " + phoneCode(str2.substring(3, 7)));
			System.out.println("User Number : " + phoneCode(str2.substring(7, 13)));
		} else if (str2.length() == 11) {
			System.out.println("Country code : "
					+ CodeMap(phoneCode(str2.substring(0, 1))));
			System.out.println("Service provider : " + phoneCode(str2.substring(1, 5)));
			System.out.println("User Number : " + str2.substring(5, 11));
		}
		else
			System.out.println("Invalid number");
	}

	public static String CodeMap(String str) {
		  Map m1 = new HashMap(); 
		  String country = ""; m1.put("91", "India");
		  m1.put("93", "Afganistan"); m1.put("61", "Australia"); m1.put("1",
		  "United States"); if (m1.containsKey(str)) { country =
		  (m1.get(str)).toString(); } else { country = "Code not found"; }
		  
		  return country;
	}
}
